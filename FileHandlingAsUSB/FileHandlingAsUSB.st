(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: FileHandling
 * File: FileHandling.st
 * Author: Bernecker + Rainer
 * Created: April 16, 2009
 ********************************************************************
 * Implementation of program Handling
 ********************************************************************)

(******************************************)
(* init program                           *)
(******************************************)
PROGRAM _INIT
Handling.Data.Step := 200;
	
END_PROGRAM

(******************************************)
(* cyclic program                         *)
(******************************************)
PROGRAM _CYCLIC

	CASE Handling.Data.Step OF	
		200:
			(*WAIT:*)
		
			IF start_reading_usb_data = TRUE THEN
				Handling.Data.Step := 201; (*CREATE_NODE_ID_LIST;*)  (*start FUBs below*)
			ELSE
				Handling.Data.Step := 200; (*WAIT;*)
			END_IF;
				
							

		201: 
			(*CREATE_NODE_ID_LIST:*)  (*Library AsUSB - Functionblock USBNodeListGet()*)
	
			UsbNodeListGet_0.enable := 1;
			UsbNodeListGet_0.pBuffer := ADR(node_id_buffer);  (*pointer to buffer - UDINT array is assigned*)
			UsbNodeListGet_0.bufferSize := SIZEOF(node_id_buffer);  (*size of node-id-buffer-array*)
			UsbNodeListGet_0.filterInterfaceClass := asusb_CLASS_MASS_STORAGE;  (*filter on mass storage devices is set*)
			UsbNodeListGet_0.filterInterfaceSubClass := 0;  (*no filer is set*)
			UsbNodeListGet_0;
								
			IF UsbNodeListGet_0.status = 0 THEN
				Handling.Data.Step := 202; (*READ_DEVICE_DATA;*)  (*FUB worked correctly => next step*)	
			ELSIF UsbNodeListGet_0.status = ERR_FUB_BUSY THEN
				Handling.Data.Step := 201; (*CREATE_NODE_ID_LIST;*)  (*FUB work asynchron => called until status isn't BUSY*)
			ELSE
				Handling.Data.Step := 255; (*ERROR_CASE;*)  (*error occured*)
			END_IF;
			
		202:
			(*READ_DEVICE_DATA: *) (*Library AsUSB - Functionblock USBNodeGet()*)
						
			UsbNodeGet_0.enable := 1;
			UsbNodeGet_0.nodeId := node_id_buffer[node];  (*specific node is read out of node_id_buffer*)
			UsbNodeGet_0.pBuffer := ADR(usb_data_buffer[node]);  (*data of specific node get stored in usb_data_buffer*)
			UsbNodeGet_0.bufferSize := SIZEOF (usb_data_buffer[node]);  (*size of specific node is read out usb_data_buffer*)
			UsbNodeGet_0;
	
			IF UsbNodeGet_0.status = 0 THEN  (*FUB worked correctly*)	
				node := node + 1;  (*next node to be read out of buffer*)
				IF node = UsbNodeListGet_0.listNodes THEN  (*last existing node is reached*)
					node := 0;
					Handling.Data.Step := 203; (*GET_DESCRIPTOR;*)  (*all nodes are read out of buffer*)
				END_IF;
		
			ELSIF UsbNodeGet_0.status = ERR_FUB_BUSY THEN
				Handling.Data.Step := 202; (*READ_DEVICE_DATA;*)  (*FUB work asynchron => called until status isn't BUSY*)
			ELSE
				Handling.Data.Step := 255; (*ERROR_CASE;*)  (*error occured*)
			END_IF;
			
		203:
			(*GET_DESCRIPTOR: *) (*Library AsUSB - Functionblock USBDescriptorGet()*)
					
			UsbDescriptorGet_0.enable := 1;
			UsbDescriptorGet_0.nodeId := node_id_buffer[node];  (*specific node is read out of node_id_buffer*) 
			UsbDescriptorGet_0.requestType := 0;  (*Request for device*)
			UsbDescriptorGet_0.descriptorType := 1;  (*Determines the device descriptor*)
			UsbDescriptorGet_0.languageId := 0;  (*for device and configuration descriptors*)
			UsbDescriptorGet_0.pBuffer := ADR(device_descriptor[node]);  (*descriptor-data of specific node get stored in device_descriptor-buffer*) 
			UsbDescriptorGet_0.bufferSize := SIZEOF(device_descriptor[node]);  (*size of specific node is read out device_descriptor-buffer*)
			UsbDescriptorGet_0;
	
			IF UsbDescriptorGet_0.status = 0 THEN  (*FUB worked correctly*)	
				node := node + 1;  (*next node to be read out of buffer*)
				IF node = UsbNodeListGet_0.listNodes THEN  (*last existing node is reached*)
					node := 0;
					Handling.Data.Step := 0; (*CREATE_FILE_DEVICE;*)  (*all nodes are read out of buffer*)
				END_IF;
	
			ELSIF UsbDescriptorGet_0.status = ERR_FUB_BUSY THEN
				Handling.Data.Step := 203; (*GET_DESCRIPTOR;*)  (*FUB work asynchron => called until status isn't BUSY*)
			ELSE
				Handling.Data.Step := 255; (*ERROR_CASE;*)  (*error occured*)
			END_IF;

		0:  (* link (create) a file device *)
			(* here you can select your filedevice for the use in the cyclic part of this program*)
			(*Select := 0;  (* local device e.g. compact flash*)
			(*Select := 1;*)  (* USB device e.g. on IF6.ST1 of a X20CP1484 *)
		
			IF Select = 0 THEN
				(* device name for different function blocks needed (choose yourself) *)
				strcpy( ADR(Handling.Data.Device), ADR('LOCAL_DEVICE') );
				(* perameter string, layout see in the online help by function block "devlink" *)
				strcpy( ADR(Handling.Data.Parameter), ADR('"/DEVICE=C:') );
			ELSIF Select = 1 THEN
				(* device name for different function blocks needed (choose yourself) *)
				strcpy( ADR(Handling.Data.Device), ADR('DEVICE1') );
				(* perameter string, layout see in the online help by function block "devlink" *)
				(*strcpy( ADR(Handling.Data.Parameter), ADR('"/DEVICE=IF6.ST1') );*)
				strcpy(ADR(Handling.Data.Parameter), ADR('/DEVICE='));  (*first part of parameter get copied to device_param-Variable*)
				strcat(ADR(Handling.Data.Parameter), ADR(usb_data_buffer[0].ifName));  (*second part get added to device_param-Variable*) 																												
	
			END_IF
	
			(* predefine filenames *)
			strcpy( ADR(Handling.Data.FileName), ADR('TestFile.txt') );
			strcpy( ADR(Handling.Data.NewFileName), ADR('CopyTestFile.txt') );
			(* predefine writedata *)
			strcpy( ADR(Handling.Data.WriteData), ADR('This is a test string! 1234567890') );
	
			Handling.Functionblock.DevLink_0.enable := 1;
			Handling.Functionblock.DevLink_0.pDevice := ADR(Handling.Data.Device);  (* name of the device, which is needed for other functionblocks *)
			Handling.Functionblock.DevLink_0.pParam := ADR(Handling.Data.Parameter);  (* devicepath *)
			Handling.Functionblock.DevLink_0;  (* call the function*)
			
			IF Handling.Functionblock.DevLink_0.status = 0 THEN  (* DevLink successful *)
				Handling.Data.Step := 1;  (* next Step*)
			ELSIF Handling.Functionblock.DevLink_0.status = ERR_FUB_BUSY THEN  (* DevLink not finished -> redo *)			
				(* Busy *)	
			ELSIF Handling.Functionblock.DevLink_0.status = fiERR_SYSTEM THEN  (* DevLink error = fiERR_SYSTEM -> detailed info with function "FileIoGetSysError" *)			
				(* get detail errorinformation *)
				Error := FileIoGetSysError();	
				(* Goto Error Step *)
				Handling.Data.Step := 255;			
			ELSE  (* Goto Error Step *)
				Handling.Data.Step := 255;
			END_IF	
		
		1:  (* Wait and comannd step *)
			IF Handling.Command.bCreateFile = 1 THEN
				(* Create a new file with the name from the variable "Handling.Data.FileName" *)
				Handling.Data.Step := 10;  (* next Step*)  	
		
			ELSIF Handling.Command.bWriteFile = 1 THEN
				(* writes the data from variable "Handling.Data.WriteData" 
				   in the file with the name from the variable "Handling.Data.FileName" *)
				Handling.Data.Step := 20;  (* next Step*)  
				
			ELSIF Handling.Command.bReadFile = 1 THEN
				(* read data to variable "Handling.Data.ReadData" 
				   from the file with the name of the variable "Handling.Data.FileName" *)
				Handling.Data.Step := 30;  (* next Step*)  
				
			ELSIF Handling.Command.bReadExFile = 1 THEN
				(* read data to variable "Handling.Data.ReadData" 
				   from the file with the name of the variable "Handling.Data.FileName" *)
				Handling.Data.Step := 40;  (* next Step*)  	

			ELSIF Handling.Command.bCopyFile = 1 THEN
				(* creates a copy with the name from the variable "Handling.Data.NewFileName"
				   from the file with the name from the variable "Handling.Data.FileName" *)
				Handling.Data.Step := 50;  (* next Step*)  	

			ELSIF Handling.Command.bRenameFile = 1 THEN
				(* renames the the file with the name from the variable "Handling.Data.FileName"
				   to the name from the variable "Handling.Data.NewFileName" *)
				Handling.Data.Step := 60;  (* next Step*)  
															
			ELSIF Handling.Command.bDeleteFile = 1 THEN
				(* deletes the file with the name from the variable "Handling.Data.FileName" *)
				Handling.Data.Step := 70;  (* next Step*)  
				 
			END_IF
		
		(***********************************************************************************************)	
		10:  (* create a new File with the selected name  *)
			Handling.Functionblock.FileCreate_0.enable := 1;
			Handling.Functionblock.FileCreate_0.pDevice := ADR(Handling.Data.Device);  (* name of the linked device *)
			Handling.Functionblock.FileCreate_0.pFile := ADR(Handling.Data.FileName);  (* name of the file *)
			Handling.Functionblock.FileCreate_0;  (* call the function*)
			
			IF Handling.Functionblock.FileCreate_0.status = 0 THEN  (* FileCreate successful *)
				Handling.Data.Step := 11;  (* next Step*)
			ELSIF Handling.Functionblock.FileCreate_0.status = ERR_FUB_BUSY THEN  (* FileCreate not finished -> redo *)			
				(* Busy *)	
			ELSE  (* Goto Error Step *)
				Handling.Data.Step := 255;
			END_IF	
			
		11:	 (* close file, because of limited number of available file handles on the system *)
			Handling.Functionblock.FileClose_0.enable := 1;
			Handling.Functionblock.FileClose_0.ident := Handling.Functionblock.FileCreate_0.ident; (* ident for FileCreate-functionblock*)
			Handling.Functionblock.FileClose_0;  (* call the function*)
			
			IF Handling.Functionblock.FileClose_0.status = 0 THEN  (* FileClose successful *)
				Handling.Data.Step := 1;  (* next Step*)
				Handling.Command.bCreateFile := 0;  (* clear command *)
			ELSIF Handling.Functionblock.FileClose_0.status = ERR_FUB_BUSY THEN  (* FileClose not finished -> redo *)			
				(* Busy *)	
			ELSE  (* Goto Error Step *)
				Handling.Data.Step := 255;
			END_IF	

		(***********************************************************************************************)	
		20:	(* open file for write access *)
			Handling.Functionblock.FileOpen_0.enable := 1;
			Handling.Functionblock.FileOpen_0.pDevice := ADR(Handling.Data.Device);  (* name of the linked device *)
			Handling.Functionblock.FileOpen_0.pFile :=  ADR(Handling.Data.FileName);  (* name of the file *)
			Handling.Functionblock.FileOpen_0.mode := fiWRITE_ONLY; (* write access *)
			Handling.Functionblock.FileOpen_0;  (* call the function*)

			IF Handling.Functionblock.FileOpen_0.status = 0 THEN  (* FileOpen successful *)
				Handling.Data.Step := 21;  (* next Step*)
			ELSIF Handling.Functionblock.FileOpen_0.status = ERR_FUB_BUSY THEN  (* FileOpen not finished -> redo *)			
				(* Busy *)	
			ELSE  (* Goto Error Step *)
				Handling.Data.Step := 255;
			END_IF	

		21: (* write data into file *)
			Handling.Functionblock.FileWrite_0.enable := 1;
			Handling.Functionblock.FileWrite_0.ident := Handling.Functionblock.FileOpen_0.ident;
			Handling.Functionblock.FileWrite_0.offset := 0; (* start at the beginning of the file *)
			Handling.Functionblock.FileWrite_0.pSrc := ADR(Handling.Data.WriteData);
			Handling.Functionblock.FileWrite_0.len := strlen( ADR(Handling.Data.WriteData) );
			Handling.Functionblock.FileWrite_0;  (* call the function*)

			IF Handling.Functionblock.FileWrite_0.status = 0 THEN  (* FileWrite successful *)
				Handling.Data.Step := 22;  (* next Step*)
			ELSIF Handling.Functionblock.FileWrite_0.status = ERR_FUB_BUSY THEN  (* FileWrite not finished -> redo *)			
				(* Busy *)	
			ELSE  (* Goto Error Step *)
				Handling.Data.Step := 255;
			END_IF	

		22:	 (* close file, because of limited number of available file handles on the system *)
			Handling.Functionblock.FileClose_0.enable := 1;
			Handling.Functionblock.FileClose_0.ident := Handling.Functionblock.FileOpen_0.ident; (* ident for FileCreate-functionblock*)
			Handling.Functionblock.FileClose_0;  (* call the function*)
			
			IF Handling.Functionblock.FileClose_0.status = 0 THEN  (* FileClose successful *)
				Handling.Data.Step := 1;  (* next Step*)
				Handling.Command.bWriteFile := 0;  (* clear command *)
			ELSIF Handling.Functionblock.FileClose_0.status = ERR_FUB_BUSY THEN  (* FileClose not finished -> redo *)			
				(* Busy *)	
			ELSE  (* Goto Error Step *)
				Handling.Data.Step := 255;
			END_IF	

		(***********************************************************************************************)	
		30:	(* open file for read access *)
			Handling.Functionblock.FileOpen_0.enable := 1;
			Handling.Functionblock.FileOpen_0.pDevice := ADR(Handling.Data.Device);  (* name of the linked device *)
			Handling.Functionblock.FileOpen_0.pFile :=  ADR(Handling.Data.FileName);  (* name of the file *)
			Handling.Functionblock.FileOpen_0.mode := fiREAD_ONLY;  (* read access *)
			Handling.Functionblock.FileOpen_0;  (* call the function*)

			IF Handling.Functionblock.FileOpen_0.status = 0 THEN  (* FileOpen successful *)
				Handling.Data.Step := 31;  (* next Step*)
			ELSIF Handling.Functionblock.FileOpen_0.status = ERR_FUB_BUSY THEN  (* FileOpen not finished -> redo *)			
				(* Busy *)	
			ELSE  (* Goto Error Step *)
				Handling.Data.Step := 255;
			END_IF	

		31: (* read data from file *)
			Handling.Functionblock.FileRead_0.enable := 1;
			Handling.Functionblock.FileRead_0.ident := Handling.Functionblock.FileOpen_0.ident;  (* ident of the previous "FileOpen" *)
			Handling.Functionblock.FileRead_0.offset := 0;  (* start at the beginning of the file *)
			Handling.Functionblock.FileRead_0.pDest := ADR(Handling.Data.ReadData);  (* adress of the destination of reaaded datas *)
			Handling.Functionblock.FileRead_0.len := SIZEOF( Handling.Data.ReadData );  (* lenght of data, which should be readed *)			
			Handling.Functionblock.FileRead_0;  (* call the function*)

			IF Handling.Functionblock.FileRead_0.status = 0 THEN  (* FileRead successful *)
				Handling.Data.Step := 32;  (* next Step*)
			ELSIF Handling.Functionblock.FileRead_0.status = ERR_FUB_BUSY THEN  (* FileRead not finished -> redo *)			
				(* Busy *)	
			ELSE  (* Goto Error Step *)
				Handling.Data.Step := 255;
			END_IF	

		32:	 (* close file, because of limited number of available file handles on the system *)
			Handling.Functionblock.FileClose_0.enable := 1;
			Handling.Functionblock.FileClose_0.ident := Handling.Functionblock.FileOpen_0.ident; (* ident for FileCreate-functionblock*)
			Handling.Functionblock.FileClose_0;  (* call the function*)
			
			IF Handling.Functionblock.FileClose_0.status = 0 THEN  (* FileClose successful *)
				Handling.Data.Step := 1;  (* next Step*)
				Handling.Command.bReadFile := 0;  (* clear command *)
			ELSIF Handling.Functionblock.FileClose_0.status = ERR_FUB_BUSY THEN  (* FileClose not finished -> redo *)			
				(* Busy *)	
			ELSE  (* Goto Error Step *)
				Handling.Data.Step := 255;
			END_IF	

		(***********************************************************************************************)	
		40:	(* open file for read access *)
			Handling.Functionblock.FileOpen_0.enable := 1;
			Handling.Functionblock.FileOpen_0.pDevice := ADR(Handling.Data.Device);  (* name of the linked device *)
			Handling.Functionblock.FileOpen_0.pFile :=  ADR(Handling.Data.FileName);  (* name of the file *)
			Handling.Functionblock.FileOpen_0.mode := fiREAD_ONLY;  (* read access *)
			Handling.Functionblock.FileOpen_0;  (* call the function*)

			IF Handling.Functionblock.FileOpen_0.status = 0 THEN  (* FileOpen successful *)
				Handling.Data.Step := 41;  (* next Step*)
			ELSIF Handling.Functionblock.FileOpen_0.status = ERR_FUB_BUSY THEN  (* FileOpen not finished -> redo *)			
				(* Busy *)	
			ELSE  (* Goto Error Step *)
				Handling.Data.Step := 255;
			END_IF	

		41: (* read data from file *)
			Handling.Functionblock.FileReadEx_0.enable := 1;
			Handling.Functionblock.FileReadEx_0.ident := Handling.Functionblock.FileOpen_0.ident;  (* ident of the previous "FileOpen" *)
			Handling.Functionblock.FileReadEx_0.offset := 0;  (* start at the beginning of the file *)
			Handling.Functionblock.FileReadEx_0.pDest := ADR(Handling.Data.ReadData);  (* adress of the destination of reaaded datas *)
			Handling.Functionblock.FileReadEx_0.len := SIZEOF( Handling.Data.ReadData );  (* lenght of data, which should be readed *)			
			Handling.Functionblock.FileReadEx_0;  (* call the function*)

			IF Handling.Functionblock.FileReadEx_0.status = 0 THEN  (* FileReadEx successful *)
				(* Handling.Functionblock.FileReadEx_0.bytesread includes the lenght of the readed data *)
				Handling.Data.Step := 42;  (* next Step*)
			ELSIF Handling.Functionblock.FileReadEx_0.status = ERR_FUB_BUSY THEN  (* FileReadEx not finished -> redo *)			
				(* Busy *)	
			ELSE  (* Goto Error Step *)
				Handling.Data.Step := 255;
			END_IF	

		42:	 (* close file, because of limited number of available file handles on the system *)
			Handling.Functionblock.FileClose_0.enable := 1;
			Handling.Functionblock.FileClose_0.ident := Handling.Functionblock.FileOpen_0.ident; (* ident for FileCreate-functionblock*)
			Handling.Functionblock.FileClose_0;  (* call the function*)
			
			IF Handling.Functionblock.FileClose_0.status = 0 THEN  (* FileClose successful *)
				Handling.Data.Step := 1;  (* next Step*)
				Handling.Command.bReadExFile := 0;  (* clear command *)
			ELSIF Handling.Functionblock.FileClose_0.status = ERR_FUB_BUSY THEN  (* FileClose not finished -> redo *)			
				(* Busy *)	
			ELSE  (* Goto Error Step *)
				Handling.Data.Step := 255;
			END_IF	
			
		(***********************************************************************************************)	
		50: (* copy a file *)
		
			Handling.Functionblock.FileCopy_0.enable := 1;
			Handling.Functionblock.FileCopy_0.option := fiOVERWRITE;  (* overwrite existing file *)
			Handling.Functionblock.FileCopy_0.pSrcDev := ADR(Handling.Data.Device);  (* name of a linked device *)
			Handling.Functionblock.FileCopy_0.pSrc := ADR(Handling.Data.FileName);  (* name of the source file *)
			Handling.Functionblock.FileCopy_0.pDestDev := ADR(Handling.Data.Device);  (* name of a linked device *)
			Handling.Functionblock.FileCopy_0.pDest := ADR(Handling.Data.NewFileName);  (* name of the new file*)
			Handling.Functionblock.FileCopy_0;  (* call the function*)
			
			IF Handling.Functionblock.FileCopy_0.status = 0 THEN  (* FileCopy successful *)
				Handling.Data.Step := 1;  (* next Step*)
				Handling.Command.bCopyFile := 0;  (* clear command *)
			ELSIF Handling.Functionblock.FileCopy_0.status = ERR_FUB_BUSY THEN  (* FileCopy not finished -> redo *)			
				(* Busy *)	
			ELSE  (* Goto Error Step *)
				Handling.Data.Step := 255;
			END_IF

		(***********************************************************************************************)	
		60: (* rename a file *)
		
			Handling.Functionblock.FileRename_0.enable := 1;
			Handling.Functionblock.FileRename_0.pDevice := ADR(Handling.Data.Device);  (* name of the linked device *)
			Handling.Functionblock.FileRename_0.pName := ADR(Handling.Data.FileName);  (* actual file name *)
			Handling.Functionblock.FileRename_0.pNewName := ADR(Handling.Data.NewFileName);  (* new file name *)
			Handling.Functionblock.FileRename_0;  (* call the function*)
			
			IF Handling.Functionblock.FileRename_0.status = 0 THEN  (* FileRename successful *)
				Handling.Data.Step := 1;  (* next Step*)
				Handling.Command.bRenameFile := 0;  (* clear command *)
			ELSIF Handling.Functionblock.FileRename_0.status = ERR_FUB_BUSY THEN  (* FileRename not finished -> redo *)			
				(* Busy *)	
			ELSE  (* Goto Error Step *)
				Handling.Data.Step := 255;
			END_IF

		(***********************************************************************************************)	
		70: (* delete a file *)
		
			Handling.Functionblock.FileDelete_0.enable := 1;
			Handling.Functionblock.FileDelete_0.pDevice := ADR(Handling.Data.Device);  (* name of a linked device *)
			Handling.Functionblock.FileDelete_0.pName := ADR(Handling.Data.FileName);  (* name of the source file *)
			Handling.Functionblock.FileDelete_0;  (* call the function*)
			
			IF Handling.Functionblock.FileDelete_0.status = 0 THEN  (* FileRename successful *)
				Handling.Data.Step := 1;  (* next Step*)
				Handling.Command.bDeleteFile := 0;  (* clear command *)
			ELSIF Handling.Functionblock.FileDelete_0.status = ERR_FUB_BUSY THEN  (* FileRename not finished -> redo *)			
				(* Busy *)	
			ELSE  (* Goto Error Step *)
				Handling.Data.Step := 255;
			END_IF

		(***********************************************************************************************)	
		255:  (* Here some error Handling has to be implemented *)
			
			

	END_CASE



END_PROGRAM
